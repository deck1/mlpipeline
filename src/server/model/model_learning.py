# %%
import pandas as pd
import numpy as np
from imblearn.over_sampling import SMOTE
from sklearn.model_selection import train_test_split
from numpy import arange
from sklearn import tree
from sklearn.svm import SVC
from xgboost import XGBClassifier
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import make_scorer, accuracy_score, f1_score
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.neighbors import KNeighborsClassifier
import datetime 
import pickle5 as pickle

# %%
pd.options.display.max_columns = None
pd.options.display.max_rows = None

# %%
s3 = boto3.resource('s3',region_name="eu-west-1",aws_access_key_id='VBLLZPX15YE3WMLBDO4A',aws_secret_access_key='v2cdWe6prisvmj1rwlEaTwUgh5NCH4gEvTYE8BJX', endpoint_url="https://kcs3.eu-west-1.klovercloud.com") 

# %%
print("----load start------")
df_raw=s3.Object('upload-zszljfwd','loan_f1.pickle').get()['Body'].read()
print("---load done----")
# %%
df=df_raw.copy()

# %%
X = df.drop('ZZ_STATUS', axis=1)
y = df['ZZ_STATUS'].values

# %%
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.20, random_state=0)
X_train, X_val, y_train, y_val = train_test_split(X_train,y_train,test_size=0.25, random_state=0)

# %%
sm = SMOTE()

# %%
X_train_smote, y_train_smote = sm.fit_resample(X_train, y_train)

# %%
def build_XGBClassifier():
    print("Starting ------ XGBClassifier")
    start_ts=datetime.datetime.now() 
    pipe = Pipeline(steps=[
        ('xg', XGBClassifier(probability=True))
    ])
    
    param_grid ={
        'xg__max_depth':[5, 10 ,20]   
    }
    model=GridSearchCV(estimator=pipe,
                             param_grid=param_grid,
                             scoring='roc_auc', 
                             n_jobs=-1,
                             pre_dispatch='2*n_jobs', 
                             cv=5, 
                             verbose=1,
                             return_train_score=False)
    
    model.fit(X_train_smote,y_train_smote)
    pkl_filename = "/home/klovercloud/app/server/model/xgboost_model.pkl"
    with open(pkl_filename, 'wb') as file:
        pickle.dump(model, file)

    X_val_np = X_val
    predicted = model.predict(X_val_np)
    proba = model.predict_proba(X_val_np)
    
    precision=precision_score(y_val, predicted, average='weighted')
    recall=recall_score(y_val, predicted, average='weighted')
    f1=f1_score(y_val, predicted, average='weighted')
    accuracy=accuracy_score(y_val, predicted)
    
    CM = confusion_matrix(y_val, predicted)
    (TN,FN,TP,FP) = (CM[0][0],CM[1][0],CM[1][1],CM[0][1])
    FPR = FP/(FP+TN)
    
    end_ts=datetime.datetime.now()
    delta=(end_ts-start_ts)
    
    COLUMNS=['Algorithm','Accuracy','Precision','Recall','F1-support','FPR','RUNTIME','PROBA','PREDICT','ESTIMATOR','BEST PARAMS']
    dic=[['XGBClassifier',accuracy,precision,recall,f1,FPR,str(delta),proba,predicted,model.best_estimator_,model.best_params_]]
    df=pd.DataFrame(dic,columns=COLUMNS)
  
    return df

# %%
print(df_XGBClassifier)